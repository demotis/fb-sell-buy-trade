<?php

// FB Requires
// Loaded from a seperate script to include all SDK files

// FB NameSpace
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;

// Simple Class to send a notification to a user about a status update
class FBUser {
    
    public $FBID;
    public $FirstName;
    public $LastName;
    public $EMail;

    function __construct()
    {
        $this->FBID = 0;
        $graphObject = $this->GetGraphObject();
        if (!$graphObject) return;
        
        $this->FBID = $graphObject->getId();
        $this->FirstName = $graphObject->getFirstName();
        $this->LastName = $graphObject->getLastName();
        $this->EMail = $graphObject->getEmail();
    }
    
    private function GetGraphObject()
    {
        if (empty($GLOBALS['FB_Session']))
            return false;
        try {
            $request = new FacebookRequest(
                $GLOBALS['FB_Session'],
                'GET',
                "/me"
            );
        
            $response = $request->execute();
            return $response->getGraphObject(GraphUser::className());
            
        } catch (FacebookRequestException $e) {
            return false;
        } catch(\Exception $ex) {
            return false;
        }

    }
}

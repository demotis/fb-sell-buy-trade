<?php

class Listing {

    public $ID;
    public $PosterFBID;
    public $CategoryID;
    public $Title;
    public $Description;
    public $Price;
    public $Offer;
    public $OfferFBID;
    public $ZipCode;
    public $DatePosted;

    public $PostingUserClass;
    public $CategoryData;
    public $ZipCodeClass;
    
    public $Images;
    
    private $ImageURLs;

    function __construct($listingID = 0)
    {
        $this->ID = $listingID == 0 ? 0 : $listingID;
        $this->PosterFBID = 0;
        $this->Title = "";
        $this->Description = "";
        $this->Price = 0;
        $this->Offer = 0;
        $this->OfferFBID = 0;
        $this->ZipCode = 0;
        $this->DatePosted = 0;
        $this->Images = array();
        
        $this->ImageURLs;

        if ($listingID > 0)
            $this->Load($listingID);
        else
            $this->CreateListing();
    }
    
    public function Load($listingID)
    {
        $GLOBALS['DatabaseAccess']->PrepareStatement("SELECT * FROM `Listings` WHERE `ID` = :ID");
        $GLOBALS['DatabaseAccess']->BindParameter(":ID", $this->ID);
        $results = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();
        
        $this->ID = $results[0]['ID'];
        $this->PosterFBID = $results[0]['PosterFBID'];
        $this->CategoryID = $results[0]['CategoryID'];
        $this->Title = $results[0]['Title'];
        $this->Description = $results[0]['Description'];
        $this->Price = $results[0]['Price'] / 100;
        $this->Offer = $results[0]['Offer'] / 100;
        $this->OfferFBID = $results[0]['OfferFBID'];
        $this->ZipCode = $results[0]['ZipCode'];
        $this->DatePosted = $results[0]['DatePosted'];
        
        $this->PostingUserClass = new User($this->PosterFBID);
        
        if ($this->CategoryID > 0)
            $this->CategoryData = new CategoryData($this->CategoryID);
        
        if ($this->ZipCode > 0)
            $this->ZipCodeClass = new ZipCode($this->ZipCode);
        
        $this->LoadImages();
    }
    
    private function CreateListing()
    {
        $this->PosterFBID = $GLOBALS['Session']->User->FBID;
        $this->ZipCode = $GLOBALS['Session']->User->ZipCode;
        $GLOBALS['DatabaseAccess']->PrepareStatement("INSERT INTO `Listings` (`PosterFBID`, `ZipCode`, `DatePosted`) VALUES (:PosterFBID, :ZipCode, NOW())");
        $GLOBALS['DatabaseAccess']->BindParameter(":PosterFBID", $this->PosterFBID);
        $GLOBALS['DatabaseAccess']->BindParameter(":ZipCode", $this->ZipCode);
        $GLOBALS['DatabaseAccess']->ExecuteQuery_Non();
        $this->ID = $GLOBALS['DatabaseAccess']->LastInsertID();
    }
    
    public function Save()
    {
        $GLOBALS['DatabaseAccess']->PrepareStatement("UPDATE `Listings` SET `CategoryID` = :CategoryID,  `Title` = :Title, `Description` = :Description, `Price` = :Price, `Offer` = :Offer, `OfferFBID` = :OfferFBID WHERE `ID` = :ID");
        $GLOBALS['DatabaseAccess']->BindParameter(":CategoryID", $this->CategoryID);
        $GLOBALS['DatabaseAccess']->BindParameter(":Title", $this->Title);
        $GLOBALS['DatabaseAccess']->BindParameter(":Description", $this->Description);
        $pricePennies = $this->Price * 100;
        $GLOBALS['DatabaseAccess']->BindParameter(":Price", $pricePennies);
        $offerPennies = $this->Offer * 100;
        $GLOBALS['DatabaseAccess']->BindParameter(":Offer", $offerPennies);
        $GLOBALS['DatabaseAccess']->BindParameter(":OfferFBID", $this->OfferFBID);
        $GLOBALS['DatabaseAccess']->BindParameter(":ID", $this->ID);
        $GLOBALS['DatabaseAccess']->ExecuteQuery_Non();
        
        $this->SaveImages();
    }
    
    private function CheckOffer($offer)
    {
        $offerPennies = $offer * 100;
        $currentOfferPennies = $this->Offer * 100;
        return $offerPennies != 0 && $offerPennies > $currentOfferPennies;
    }
    
    public function MakeOffer($offer)
    {
        if (!$this->CheckOffer($offer))
            return false;
        $this->Offer = $offer;
        $this->OfferFBID = $GLOBALS['Session']->User->FBID;
        $this->Save();
        return true;
    }
    
    public function GetOfferDetails()
    {
        $detailsHTML = "";
        if ($this->PosterFBID == $GLOBALS['Session']->User->FBID) return $detailsHTML;

    }

    public function LoadImages()
    {
        $GLOBALS['DatabaseAccess']->PrepareStatement("SELECT `FileName` FROM `ListingImages` WHERE `ListingID` = :ID");
        $GLOBALS['DatabaseAccess']->BindParameter(":ID", $this->ID);
        $results = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();
        foreach ($results as $FileName)
            $this->Images[] = $FileName['FileName'];
    }
    
    public function AddImage($fileName)
    {
        $this->Images[] = $fileName;
    }
    
    public function GetImageURLs()
    {
        if (!empty($this->ImageURLs)) return $this->ImageURLs;
        
        $this->ImageURLs = array();
        if (empty($this->Images))
            $this->ImageURLs[] = "NoImage.jpg";
        else
            foreach ($this->Images as $image)
                $this->ImageURLs[] = "./PageImages/{$image}";
        return $this->GetImageURLs();
    }
    
    public function SaveImages()
    {
        if (empty($this->Images)) return;
        
        $GLOBALS['DatabaseAccess']->PrepareStatement("INSERT IGNORE INTO `ListingImages` (`ListingID`, `FileName`) VALUES (:ID, :FileName)");
        $GLOBALS['DatabaseAccess']->BindParameter(":ID", $this->ID);
        
        $fileNameBind = "";
        $GLOBALS['DatabaseAccess']->BindParameter(":FileName", $fileNameBind);
        
        foreach ($this->Images as $FileName)
        {
            $fileNameBind = $FileName;
            $GLOBALS['DatabaseAccess']->ExecuteQuery_Non();
        }
    }
    
    public function DisplayTitle()
    {
        return "<h3 style='margin-top:0px;'><span class='label label-info col-sm-12'>{$this->Title}</span></h3>";
    }
    
    public function AddComment($rawComment)
    {
        $safeComment = strip_tags($rawComment);
        if (empty($safeComment)) return;
        
        $GLOBALS['DatabaseAccess']->PrepareStatement("INSERT INTO `ListingComments` (`PosterFBID`, `ListingID`, `DatePosted`, `Comment`) VALUES (:PosterFBID, :ListingID, NOW(), :Comment)");
        $GLOBALS['DatabaseAccess']->BindParameter(":PosterFBID", $GLOBALS['Session']->User->FBID);
        $GLOBALS['DatabaseAccess']->BindParameter(":ListingID", $this->ID);
        $GLOBALS['DatabaseAccess']->BindParameter(":Comment", $safeComment);
        $GLOBALS['DatabaseAccess']->ExecuteQuery_Non();
    }
    
    public function DisplayComments()
    {
        $comments = "";
        
        $GLOBALS['DatabaseAccess']->PrepareStatement("SELECT `lc`.`DatePosted`, `lc`.`Comment`, `ua`.`FirstName`, `ua`.`LastName`, `ua`.`FBID` FROM `ListingComments` `lc` JOIN `UserAccounts` `ua` ON `ua`.`FBID` = `lc`.`PosterFBID` WHERE `lc`.`ListingID` = :ID ORDER BY `lc`.`DatePosted`");
        $GLOBALS['DatabaseAccess']->BindParameter(":ID", $this->ID);
        $results = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();
        foreach ($results as $commentData)
            $comments .= '<tr><td style="width: 70px;">'
                      . "<img src='http://graph.facebook.com/{$commentData['FBID']}/picture' class='img-thumbnail img-responsive' />"
                      . '</td><td><table class="table table-condensed"><thead><tr>'
                      . "<th><a href='https://www.facebook.com/{$commentData['FBID']}' target='_blank'>{$commentData['FirstName']} {$commentData['LastName']}</a></th>"
                      . "<td class='text-right text-muted'>{$commentData['DatePosted']}</td>"
                      . '</tr></thead><tbody><tr><td colspan="2">'
                      . $commentData['Comment']
                      . '</td></tr></tbody></table></td></tr>';
        
        if (empty($comments))
            $comments = "There are No Comments for this listing.";
            
        return $comments;
    }
}
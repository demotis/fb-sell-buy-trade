<?php

// FB Requires
// Loaded from a seperate script to include all SDK files

// FB NameSpace
use Facebook\FacebookSession;
use Facebook\FacebookRequest;

// Simple Class to send a notification to a user about a status update
class FBNotification {

    function __construct()
    {
    }
    
    public function Send()
    {
        if (!$GLOBALS['FB_Session']) return;
        /* PHP SDK v4.0.0 */
        /* make the API call */
        $request = new FacebookRequest(
            $GLOBALS['FB_Session'],
            'POST',
            "/856012824418838/notifications",
            array (
                'access_token' => $GLOBALS['FB_AppAccessToken'],
                'href' => 'index.php',
                'template' => 'This is a test message'
            )
        );
        
        $response = $request->execute();
        $graphObject = $response->getGraphObject();
        /* handle the result */
    }
}
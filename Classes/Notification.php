<?php

class Notifications {
    
    private $FBID;
    
    function __construct($fbID)
    {
        $this->FBID = $fbID;
    }
    
    public function MarkInactive($lid) {
        $sql = "UPDATE `Notifications` SET `IsActive` = 0 WHERE `FBID` = :FBID AND `ListingID` = :LID";
        $GLOBALS['DatabaseAccess']->PrepareStatement($sql);
        $GLOBALS['DatabaseAccess']->BindParameter(":FBID", $this->FBID);
        $GLOBALS['DatabaseAccess']->BindParameter(":LID", $lid);
        $GLOBALS['DatabaseAccess']->ExecuteQuery_Non();
    }
    
    public function Get() {
        $sql = "SELECT `ListingID` FROM `Notifications` WHERE `FBID` = :FBID AND `IsActive` = 1";
        $GLOBALS['DatabaseAccess']->PrepareStatement($sql);
        $GLOBALS['DatabaseAccess']->BindParameter(":FBID", $this->FBID);
        $results = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();
        return count($results) > 0 ? $results : array();
    }
    
    public function Create($lid, $notficationType) {
        $sql = "INSERT INTO `Notifications` (`FBID`, `ListingID`, ``) VALUES (:FBID, :LID) ON DUPLICATE KEY UPDATE `IsActive` = 1";
        $GLOBALS['DatabaseAccess']->PrepareStatement($sql);
        $GLOBALS['DatabaseAccess']->BindParameter(":FBID", $this->FBID);
        $GLOBALS['DatabaseAccess']->BindParameter(":LID", $lid);
        $GLOBALS['DatabaseAccess']->ExecuteQuery_Non();
    }
    
    
}

abstract class NotificationType
{
    public static $NewComment = 1;
    public static $NewOffer = 2;
    public static $OfferAccepted = 3;
    
    public static $NotificationText = array(
        1 => "New Comment to Review",
        2 => "New Offer on Item",
        3 => "Your Offer Accepted"
    );
}

$today = NotificationType::$NewComment;
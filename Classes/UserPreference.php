<?php

class UserPreference {

    public $FBID;
    public $Name;
    public $Value;

    function __construct($fbID, $name, $value)
    {
        $this->FBID = $fbID;
        $this->Name = $name;
        $this->Value = $value;
    }
    
    public function Save()
    {
        $GLOBALS['DatabaseAccess']->PrepareStatement("INSERT INTO `UserPreferences` (`FBID`, `Name`, `Value`) VALUES (:FBID, :Name, :Value) ON DUPLICATE KEY UPDATE `Value` = :Value");
        $GLOBALS['DatabaseAccess']->BindParameter(":FBID", $this->FBID);
        $GLOBALS['DatabaseAccess']->BindParameter(":Name", $this->Name);
        $GLOBALS['DatabaseAccess']->BindParameter(":Value", $this->Value);
        $GLOBALS['DatabaseAccess']->ExecuteQuery_Non();
    }
    
}
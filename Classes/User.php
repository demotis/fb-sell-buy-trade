<?php

class User {

    public $FBID;
    public $FirstName;
    public $LastName;
    public $EMail;
    public $ZipCode;
    public $IsBanned;
    
    public $ZipCodeClass;
    
    public $Preferences;

    function __construct($fbID = "")
    {
        $this->FBID = "";
        $this->FirstName = "";
        $this->LastName = "";
        $this->EMail = "";
        $this->ZipCode = "";
        $this->IsBanned = "";
        $this->Preferences = array();

        if (!empty($fbID))
            $this->Load($fbID);
            
    }
    
    public function Load($fbID)
    {
        $GLOBALS['DatabaseAccess']->PrepareStatement("SELECT * FROM `UserAccounts` WHERE `FBID` = :FBID");
        $GLOBALS['DatabaseAccess']->BindParameter(":FBID", $fbID);
        $userData = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();
        if (count($userData) < 1)
            return;
        $this->FBID = $userData[0]['FBID'];
        $this->FirstName = $userData[0]['FirstName'];
        $this->LastName = $userData[0]['LastName'];
        $this->EMail = $userData[0]['EMail'];
        $this->ZipCode = $userData[0]['ZipCode'];
        $this->IsBanned = $userData[0]['IsBanned'];
        
        if ($this->ZipCode > 0)
            $this->ZipCodeClass = new ZipCode($this->ZipCode);
        
        $this->LoadPreferences();
    }
    
    private function LoadPreferences()
    {
        $GLOBALS['DatabaseAccess']->PrepareStatement("SELECT `Name`, `Value` FROM `UserPreferences` WHERE `FBID` = :FBID");
        $GLOBALS['DatabaseAccess']->BindParameter(":FBID", $this->FBID);
        $prefData = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();
        if (count($prefData) < 1)
            return;
        foreach ($prefData as $key => $value)
            $this->Preferences[$key] = new UserPreference($this->FBID, $key, $value);
        
    }
    
    public function Save()
    {
        $GLOBALS['DatabaseAccess']->PrepareStatement("INSERT INTO `UserAccounts` (`FBID`, `FirstName`, `LastName`, `EMail`, `ZipCode`, `IsBanned`) VALUES (:FBID, :FirstName, :LastName, :EMail, :ZipCode, :IsBanned) ON DUPLICATE KEY UPDATE `FirstName` = :FirstName, `LastName` = :LastName, `EMail` = :EMail, `ZipCode` = :ZipCode, `IsBanned` = :IsBanned");
        $GLOBALS['DatabaseAccess']->BindParameter(":FBID", $this->FBID);
        $GLOBALS['DatabaseAccess']->BindParameter(":FirstName", $this->FirstName);
        $GLOBALS['DatabaseAccess']->BindParameter(":LastName", $this->LastName);
        $GLOBALS['DatabaseAccess']->BindParameter(":EMail", $this->EMail);
        $GLOBALS['DatabaseAccess']->BindParameter(":ZipCode", $this->ZipCode);
        $GLOBALS['DatabaseAccess']->BindParameter(":IsBanned", $this->IsBanned);
        $GLOBALS['DatabaseAccess']->ExecuteQuery_Non();
    }
    
    public function GetFBImageUrl()
    {
        return "http://graph.facebook.com/{$this->FBID}/picture";
    }

}
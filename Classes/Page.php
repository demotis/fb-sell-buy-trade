<?php

class Page {

    public $BodyTemplate;

    function __construct($bodyTemplate = "")
    {
        $this->BodyTemplate = !empty($bodyTemplate) ?
            $bodyTemplate :
            "Home";
    }

    public function IncludeJS($jsFile)
    {
        if (!file_exists($GLOBALS['Dir_JS'] . $jsFile))
            return;
        echo "<script src='{$GLOBALS['Page_JS']}{$jsFile}'></script>";
    }
    
    public function RequireJS($jsFile)
    {
        if (!file_exists($GLOBALS['Dir_JS'] . $jsFile))
            die("Error loading required file : " . $jsFile);
        echo "<script src='{$GLOBALS['Page_JS']}{$jsFile}'></script>";
    }

    public function IncludeCSS($cssFile)
    {
        if (!file_exists($GLOBALS['Page_CSS'] . $cssFile))
            return;
        echo "<link href='{$GLOBALS['Page_CSS']}{$cssFile}' rel='stylesheet'>";
    }

    public function RequireCSS($cssFile)
    {
        if (!file_exists($GLOBALS['Page_CSS'] . $cssFile))
            die("Error loading required file : " . $cssFile);
        echo "<link href='{$GLOBALS['Page_CSS']}{$cssFile}' rel='stylesheet'>";
    }
    
    public function IncludeTemplate($GUIPage)
    {
        if (!file_exists($GLOBALS['Dir_GUI'] . $GUIPage . ".php"))
            die("Error loading required file : " . $GUIPage);
        require $GLOBALS['Dir_GUI'] . $GUIPage . ".php";
    }

    public function Render()
    {
        // Renders the templates in a basic HTML5 Boilerplate
        ?>

<!DOCTYPE html>
<html lang="en">
    <?php $this->IncludeTemplate("Common-Head"); ?>
    <body>
        <div class="container">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <?php $this->IncludeTemplate("NavBar"); ?>
                </div>
                <div class="panel-body" style="background-color: #eaeaea;">
                    <?php
                        foreach ($GLOBALS['Alerts'] as $Alert)
                            $Alert->Render();
                    ?>
                    <div class="row">
                        <div class="col-sm-12 col-md-3">
                            <?php $this->IncludeTemplate("Common-SideBar"); ?>
                        </div>
                        <div class="col-sm-12 col-md-9">
                            <?php $this->IncludeTemplate($this->BodyTemplate); ?>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
        <?php $this->IncludeTemplate("Common-Footer"); ?>
    </body>
</html>
        
        
        <?php
    }
}
<?php

/**
 *
 */
class Categories {

    private $Roots;
    private $Children;
    
    function __construct()
    {
        $this->Roots = array();
        $this->Children = array();
    }

    public function GetRoots()
    {
        if (!empty($this->Roots))
            return $this->Roots;
        $GLOBALS['DatabaseAccess']->PrepareStatement("SELECT * FROM `Categories` WHERE `ParentID` = 0");
        $this->Roots = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();
        return $this->Roots;
    }
    
    public function GetChildren($rootID)
    {
        if (!empty($this->Children[$rootID]))
            return $this->Children[$rootID];
        $GLOBALS['DatabaseAccess']->PrepareStatement("SELECT * FROM `Categories` WHERE `ParentID` = :rootID ORDER BY `Label`");
        $GLOBALS['DatabaseAccess']->BindParameter(":rootID", $rootID);
        $this->Children[$rootID] = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();
        return $this->Children[$rootID];
    }
}
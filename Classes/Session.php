<?php

class Session {

    private $FBID;
    public $User;
    
    public $ActiveZipCode;
    public $ActiveDistance;
    public $CurrentSearchZipCodes;
    public $ActiveSearchCategoryID;
    

    function __construct()
    {
        ini_set('session.cookie_lifetime', 60 * 60 * 24 * 7);
        ini_set('session.gc_maxlifetime', 60 * 60 * 24 * 7);
        ini_set('session.save_path', '/var/www/fbclass/Sessions');
        session_start();
        
        $this->FBID = "";
        $this->ActiveZipCode = "0";
        $this->ActiveDistance = "1";
        $this->CurrentSearchZipCodes = "";
        $this->ActiveSearchCategoryID = 0;
        if (isset($_SESSION['FBID']))
            $this->ValidateSession();

    }
    
    private function ValidateSession()
    {
        $this->LoadFromSESSION();
        
        // Save a little time
        if (!isset($this->User))
            return $this->Logout();
            
        // Load the FB User
        $fbUser = new FBUser();
        
        if ($fbUser->FBID != $this->FBID)
            return $this->Logout();
    }
    
    private function LoadFromSESSION()
    {
        $this->FBID = $_SESSION['FBID'];
        $this->LoadUser($_SESSION['FBID']);

        $this->ActiveZipCode = $_SESSION['ActiveZipCode'] != 0 ?
            $_SESSION['ActiveZipCode'] :
            $this->User->ZipCode;
        $this->ActiveDistance = $_SESSION['ActiveDistance'] != 1 ?
            $_SESSION['ActiveDistance'] :
            35;
        $this->CurrentSearchZipCodes = $_SESSION['CurrentSearchZipCodes'] != "" ?
            $_SESSION['CurrentSearchZipCodes'] :
            (isset($this->User->ZipCodeClass) ?
                $this->User->ZipCodeClass->GetZipsInRange($this->ActiveDistance) :
                "");
        $this->ActiveSearchCategoryID = $_SESSION['ActiveSearchCategoryID'];
    }
    
    public function SetActiveZipCode($newZipCode)
    {
        $this->ActiveZipCode = $newZipCode;
        $_SESSION['ActiveZipCode'] = $this->ActiveZipCode;
        $this->UpdateSearchZipCodes();
    }
    
    public function GetZipCodeCSV()
    {
        
    }

    public function SetActiveDistance($newDistance)
    {
        $this->ActiveDistance = $newDistance;
        $_SESSION['ActiveDistance'] = $this->ActiveDistance;
        $this->UpdateSearchZipCodes();
    }

    public function SetActiveSearchCategoryID($newCatID)
    {
        $this->ActiveSearchCategoryID = $newCatID;
        $_SESSION['ActiveSearchCategoryID'] = $this->ActiveSearchCategoryID;
    }

    private function UpdateSearchZipCodes()
    {
        $altZip = $this->User->ZipCode == $this->ActiveZipCode ?
            "" :
            $this->ActiveZipCode;
        $this->CurrentSearchZipCodes = isset($this->User->ZipCodeClass) ?
            $this->User->ZipCodeClass->GetZipsInRange($this->ActiveDistance, $altZip) :
            "";
        $_SESSION['CurrentSearchZipCodes'] = $this->CurrentSearchZipCodes;
    }
    
    public function SetSessionID($fbID)
    {
        $this->FBID = $fbID;
        $this->LoadUser($fbID);
        $this->SetupSession();
    }
    
    public function SetSessionByUser($userData)
    {
        $this->User = $userData;
        $this->FBID = $userData->FBID;
        $this->SetupSession();
    }
    
    private function SetupSession()
    {
        $_SESSION['FBID'] = $this->FBID;

        $_SESSION['ActiveZipCode'] = $this->User->ZipCode;
        $_SESSION['ActiveDistance'] = 35;
        $_SESSION['CurrentSearchZipCodes'] = isset($this->User->ZipCodeClass) ?
            $this->User->ZipCodeClass->GetZipsInRange($_SESSION['ActiveDistance']) :
            "";
        $_SESSION['ActiveSearchCategoryID'] = 0;

        $this->ActiveZipCode = $_SESSION['ActiveZipCode'];
        $this->ActiveDistance = $_SESSION['ActiveDistance'];
        $this->CurrentSearchZipCodes = $_SESSION['CurrentSearchZipCodes'];
    }
    
    public function LoadUser($fbID)
    {
        $tempUser = new User($fbID);
        if($tempUser->IsBanned)
            die("You User Account has been banned");
        $this->FBID = $fbID;
        $this->User = $tempUser;
    }
    
    public function Logout()
    {
        session_destroy();
        unset($_REQUEST['Form']);
        
        $this->FBID = "";
        $this->ActiveZipCode = "0";
        $this->ActiveDistance = "1";
        $this->CurrentSearchZipCodes = "";
        $this->ActiveSearchCategoryID = 0;
        $this->User = null;
    }
    
}
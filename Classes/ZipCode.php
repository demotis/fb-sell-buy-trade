<?php


/**
 *  ZipCode
 */

class ZipCode
{
    private $ZipCode;
    private $Lat;
    private $Lon;

    private $print_name;

    public function __construct($zipCode) 
    {
        $this->ZipCode = $this->sanitizeZip($zipCode);
        $this->print_name = $this->ZipCode;
        $this->setPropertiesFromDb();
    }

    public function __toString()
    {
        return $this->print_name;
    }
    
    private function calcDistanceSql($location)
    {
        $sql = 'SELECT 3956 * 2 * ATAN2(SQRT(POW(SIN((RADIANS(t2.Lat) - '
              .'RADIANS(t1.Lat)) / 2), 2) + COS(RADIANS(t1.Lat)) * '
              .'COS(RADIANS(t2.Lat)) * POW(SIN((RADIANS(t2.Lon) - '
              .'RADIANS(t1.Lon)) / 2), 2)), '
              .'SQRT(1 - POW(SIN((RADIANS(t2.Lat) - RADIANS(t1.Lat)) / 2), 2) + '
              .'COS(RADIANS(t1.Lat)) * COS(RADIANS(t2.Lat)) * '
              .'POW(SIN((RADIANS(t2.Lon) - RADIANS(t1.Lon)) / 2), 2))) '
              .'AS `Miles` '
              ."FROM `ZipCodes` t1 INNER JOIN `ZipCodes` t2 "
              ."WHERE `t1`.`ZipCode` = :zipCode "
              ."AND `t2`.`ZipCode` = :zipTo";
        $GLOBALS['DatabaseAccess']->PrepareStatement($sql);
        $GLOBALS['DatabaseAccess']->BindParameter(":zipCode", $this->ZipCode);
        $zipTo = $this->sanitizeZip($location);
        $GLOBALS['DatabaseAccess']->BindParameter(":zipTo", $zipTo);
        $results = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();

        if (empty($results))
            throw new Exception("Record does not exist calculating distance between {$this->ZipCode} and {$zip_to}");

        return $results[0]['Miles'];
    }

    public function getDistanceTo($zip)
    {
        return $this->calcDistanceSql($zip);
    }
    
    public function GetZipsInRange($rangeTo, $fromZipCode = "")
    {
        if ($fromZipCode != "")
        {
            try {
                $zipCodeClass = new ZipCode($fromZipCode);
                return $zipCodeClass->GetZipsInRange($rangeTo);
            } catch (Exception $e) {
                return array();
            }
        }

        
        $rangeTo = $rangeTo < 5 ? 5 : $rangeTo;
        
        $sql = "SELECT 3956 * 2 * ATAN2(SQRT(POW(SIN((RADIANS(:Lat) - "
              .'RADIANS(z.Lat)) / 2), 2) + COS(RADIANS(z.Lat)) * '
              ."COS(RADIANS(:Lat)) * POW(SIN((RADIANS(:Lon) - "
              ."RADIANS(z.Lon)) / 2), 2)), SQRT(1 - POW(SIN((RADIANS(:Lat) - "
              ."RADIANS(z.Lat)) / 2), 2) + COS(RADIANS(z.Lat)) * "
              ."COS(RADIANS(:Lat)) * POW(SIN((RADIANS(:Lon) - "
              ."RADIANS(z.Lon)) / 2), 2))) AS `miles`, z.ZipCode as `ZipCode` FROM `ZipCodes` z "
              ."WHERE ZipCode <> :ZipCode " 
              ."AND Lat BETWEEN ROUND(:Lat - (25 / 69.172), 4) "
              ."AND ROUND(:Lat + (25 / 69.172), 4) "
              ."AND Lon BETWEEN ROUND(:Lon - ABS(25 / COS(:Lat) * 69.172)) "
              ."AND ROUND(:Lon + ABS(25 / COS(:Lat) * 69.172)) "
              ."AND 3956 * 2 * ATAN2(SQRT(POW(SIN((RADIANS(:Lat) - "
              ."RADIANS(z.Lat)) / 2), 2) + COS(RADIANS(z.Lat)) * "
              ."COS(RADIANS(:Lat)) * POW(SIN((RADIANS(:Lon) - "
              ."RADIANS(z.Lon)) / 2), 2)), SQRT(1 - POW(SIN((RADIANS(:Lat) - "
              ."RADIANS(z.Lat)) / 2), 2) + COS(RADIANS(z.Lat)) * "
              ."COS(RADIANS(:Lat)) * POW(SIN((RADIANS(:Lon) - "
              ."RADIANS(z.Lon)) / 2), 2))) <= :RangeTo "
              //."AND 3956 * 2 * ATAN2(SQRT(POW(SIN((RADIANS(:Lat) - "
              //."RADIANS(z.Lat)) / 2), 2) + COS(RADIANS(z.Lat)) * "
              //."COS(RADIANS(:Lat)) * POW(SIN((RADIANS(:Lon) - "
              //."RADIANS(z.Lon)) / 2), 2)), SQRT(1 - POW(SIN((RADIANS(:Lat) - "
              //."RADIANS(z.Lat)) / 2), 2) + COS(RADIANS(z.Lat)) * "
              //."COS(RADIANS(:Lat)) * POW(SIN((RADIANS(:Lon) - "
              //."RADIANS(z.Lon)) / 2), 2))) >= $range_from "
              ."ORDER BY 1 ASC";

        $GLOBALS['DatabaseAccess']->PrepareStatement($sql);
        $GLOBALS['DatabaseAccess']->BindParameter(":Lat", $this->Lat);
        $GLOBALS['DatabaseAccess']->BindParameter(":Lon", $this->Lon);
        $GLOBALS['DatabaseAccess']->BindParameter(":ZipCode", $this->ZipCode);
        $GLOBALS['DatabaseAccess']->BindParameter(":RangeTo", $rangeTo);
        $results = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();

        if (empty($results))
            throw new Exception("No records found between the zipcode {$this->ZipCode} and the range of  {$rangeTo}");

        $return = array();
        $return[$this->ZipCode] = "0.00";
        foreach ($results as $row)
            $return[$row['ZipCode']] = number_format($row['miles'], 2) ;

        return $return;
    }

    static function isValidZip($zip)
    { 
        return preg_match('/^[0-9]{5}/', $zip);
    }

    private function sanitizeZip($zip)
    {
        return preg_replace("/[^0-9]/", '', $zip);
    }
    
    private function setPropertiesFromArray($a)
    {    
        if (!is_array($a))
            throw new Exception("Argument is not an array");

        foreach ($a as $key => $value)
            $this->$key = $value;

    }
    
    private function setPropertiesFromDb()
    {
        $sql = "SELECT * FROM `ZipCodes` t "
              ."WHERE `ZipCode` = :ZipCode LIMIT 1";

        $GLOBALS['DatabaseAccess']->PrepareStatement($sql);
        $GLOBALS['DatabaseAccess']->BindParameter(":ZipCode", $this->ZipCode);
        $results = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();

        if (empty($results))
            throw new Exception("{$this->print_name} was not found in the database.");

        $this->setPropertiesFromArray($results[0]);
    }
}
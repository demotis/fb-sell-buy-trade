<?php

/**
 *
 */
class DatabaseAccess {

    private $PDO;
    private $Query;

    function __construct($host, $db, $uname, $pword) {
        try {
            $this->PDO = new PDO("mysql:host={$host};dbname={$db}", $uname, $pword);
            $this->PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            $this->PDO = null;
            die($e->getMessage());
            return Error('DB ERROR: ' . $e->getMessage());
        }
    }

    function __destruct() {
        $this->Query = null;
        $this->PDO = null;
    }

    public function PrepareStatement($sqlString) {
        $this->Query = $this->PDO->prepare($sqlString);
    }

    public function BindParameter($paramName, &$paramHolder) {
        $this->Query->bindParam($paramName, $paramHolder);
    }

    public function UpdateTableViaArrayByID($tableName, $keyValues) {
        if (isset($keyValues['iface']))
            unset($keyValues['iface']);

        $update = "UPDATE `{$tableName}` SET ";
        $sets = "";
        $where = "";
        foreach ($keyValues as $field => $value) {
            $param = str_replace("!", "", $field);
            if (strtoupper($field) == "ID")
                $where = " WHERE `{$field}` = :{$param}";
            else
                $sets .= "`{$field}` = :{$param}, ";
        }
        $this->PrepareStatement($update . substr($sets, 0, -2) . $where);

        foreach ($keyValues as $field => $value) {
            $param = str_replace("!", "", $field);
            $this->BindParameter(":{$param}", $keyValues[$field]);
        }

        return $this->ExecuteQuery_Non();
    }

    public function ExecuteQuery_Get() {
        try {
            $this->Query->execute();
            return $this->Query->fetchAll(PDO::FETCH_ASSOC);
        } catch(PDOException $e) {
            $this->PDO = null;
            die('ERROR: ' . $e->getMessage());
        }
    }

    public function ExecuteQuery_Non() {
        try {
            $this->Query->execute();
            return $this->Query->rowCount();
        } catch(PDOException $e) {
            $this->PDO = null;
            return die('ERROR: ' . $e->getMessage());
        }
    }

    public function RowCount() {
        try {
            $this->Query->rowCount();
        } catch(PDOException $e) {
            $this->PDO = null;
            return 0;
        }
    }

    public function LastInsertID() {
        try {
            return $this->PDO->lastInsertId();
        } catch(PDOException $e) {
            $this->PDO = null;
            return 0;
        }
    }

    public function SimpleGet($sql)
    {
        try {
            $Query = $this->PDO->prepare($sql);
            $Query->execute();
            return $Query->fetchAll(PDO::FETCH_ASSOC);
        } catch(PDOException $e) {
            $this->PDO = null;
            return Error('ERROR: ' . $e->getMessage());
        }
    }

    public function SimpleNon($sql)
    {
        try {
            $Query = $this->PDO->prepare($sql);
            $Query->execute();
            return $Query->rowCount();
        } catch(PDOException $e) {
            $this->PDO = null;
            return Error('ERROR: ' . $e->getMessage());
        }
    }
}
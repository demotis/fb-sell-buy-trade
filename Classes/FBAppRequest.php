<?php

// FB Requires
// Loaded from a seperate script to include all SDK files

// FB NameSpace
use Facebook\FacebookSession;
use Facebook\FacebookRequest;

// Simple Class to send a notification to a user about a status update
class FBAppRequest {

    function __construct()
    {
    }
    
    public function Send()
    {
        if (!$GLOBALS['FB_Session']) return;
        /* PHP SDK v4.0.0 */
        /* make the API call */
        $request = new FacebookRequest(
            $GLOBALS['FB_Session'],
            'POST',
            "/me/apprequests",
            array (
                'message' => 'This is a test message',
                'to' => '856012824418838'
            )
        );
        
        $response = $request->execute();
        $graphObject = $response->getGraphObject();
        /* handle the result */
        
        var_dump($response);
        echo "<br><br>";
        var_dump($graphObject);
    }
}
<?php

/**
 *
 */
class CategoryData {

    public $ParentInfo;
    private $CategoryInfo;
    
    function __construct($catID)
    {
        $this->CategoryInfo = $this->GetCategoryData($catID);
        $this->ParentInfo = $this->GetCategoryData($this->CategoryInfo['ParentID']);
    }

    function GetCategoryData($catID)
    {
        $GLOBALS['DatabaseAccess']->PrepareStatement("SELECT * FROM `Categories` WHERE `ID` = :catID");
        $GLOBALS['DatabaseAccess']->BindParameter(":catID", $catID);
        $results = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();
        if (count($results) == 0)
            die("Error getting CatID {$catID}");
        return $results[0];
    } 
    
    public function GetBreadCrumb()
    {
        $breadCrumb = '<ol class="breadcrumb"><li>'
                    . $this->ParentInfo['Label']
                    . '</li><li class="active"><a href="?Page=ItemList&CatID='
                    . $this->CategoryInfo['ID'] . '">'
                    . $this->CategoryInfo['Label']
                    . '</a></li></ol>';
        return $breadCrumb;
    }
    
    public function GetListings($page = 0)
    {
        //Get Listings Limit to 25 per Page
        
        $sql = "SELECT `ID` FROM `Listings` WHERE `CategoryID` = :CategoryID AND `ZipCode` IN (" . implode(",", array_keys($GLOBALS['Session']->CurrentSearchZipCodes)) . ")ORDER BY `DatePosted` DESC LIMIT 10";
        $GLOBALS['DatabaseAccess']->PrepareStatement($sql);
        
        $catID = $this->CategoryInfo['ID'];
        $GLOBALS['DatabaseAccess']->BindParameter(":CategoryID", $this->CategoryInfo['ID']);
        
        $results = $GLOBALS['DatabaseAccess']->ExecuteQuery_Get();
        
        $listings = "";
        foreach ($results as $listingID)
        {
            $listing = new Listing($listingID['ID']);
            
            $imageSRC = !empty($listing->Images) ? "Thumbnail.php?img={$listing->Images[0]}" : "NoImage.jpg";
            $listings .= "<a href='?Page=ShowListing&lid={$listing->ID}'><div class='row panel' style='padding: 5px; margin-bottom: 10px; height: 117px;'>"
                       . "    <div class='col-md-2' style=''>"
                       . "        <img src='{$imageSRC}' class='img-responsive img-thumbnail' />"
                       . "    </div>"
                       . "    <div class='col-md-10' style=''>"
                       . "        <div class='row' style='margin-bottom: 5px;'><div class='col-sm-11' style='height: 29px; overflow: hidden;'><p class='lead' style='margin-bottom: 0px'>{$listing->Title}</p></div><div class='col-sm-1'><span class='badge pull-right'>\${$listing->Price}</span></div></div>"
                       . "        <div class='row' style='height: 40px; overflow: hidden; margin-bottom: 5px;'>{$listing->Description}</div>"
                       . "        <div class='row'>"
                       . "            <div class='col-md-6'>by:{$listing->PostingUserClass->FirstName} {$listing->PostingUserClass->LastName}</div>"
                       . "            <div class='col-md-6 text-right'>Comments <span class='badge'>0</span></div>"
                       . "        </div>"
                       . "   </div>"
                       . "</div></a>";
        }

        $listings = "<div class='container-fluid'>" . $listings . "<div>";
        
        return $listings;
    }
}
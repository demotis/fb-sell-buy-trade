<?php

/**
 *
 */
class Alert {

    private $Class;
    private $Message;

    function __construct($message, $class = "info")
    {
        $this->Message = $message;
        $this->Class = $class;
    }
    
    public function Render()
    { ?>
        <div class="alert alert-<?php echo $this->Class; ?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <?php echo $this->Message; ?>
        </div>
    <?php }
}
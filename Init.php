<?php

///
// Main entry point for a live script. The file is what sets up the working environment and security
//     Global functions for including files is here also.
///

// Get this Dir and then remove the current subfolder name length
$GLOBALS['Dir_Base'] = realpath(dirname(__FILE__)) . "/";

require_once 'Config.php';
require_once 'AutoLoad.php';

if ($GLOBALS['Debug']) {
    error_reporting(E_ALL);
    ini_set('display_errors', 'on');
}

// Load up the Includes
$files = glob($GLOBALS['Dir_Includes'] . '*.{php}', GLOB_BRACE);
foreach($files as $file)
    RequireInclude(basename($file, ".php"));

// Load up the Classes
$files = glob($GLOBALS['Dir_Classes'] . '*.{php}', GLOB_BRACE);
foreach($files as $file)
    RequireClass(basename($file, ".php"));

$GLOBALS['DatabaseAccess'] = new DatabaseAccess(
    $GLOBALS['MySQL_Host'],
    $GLOBALS['MySQL_Database'],
    $GLOBALS['MySQL_User'],
    $GLOBALS['MySQL_Password']
);

$GLOBALS['Session'] = new Session();


function RequireInclude($var) {
    require_once $GLOBALS['Dir_Includes'] . $var . ".php";
}

function RequireClass($var) {
    require_once $GLOBALS['Dir_Classes'] . $var . ".php";
}


function RequireProcessor($var) {
    require_once $GLOBALS['Dir_Forms'] . $var . ".php";
}



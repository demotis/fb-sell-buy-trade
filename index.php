<?php

    // Init the session and load required files
    require('Init.php');
    
    if (isset($_REQUEST['Form']))
        RequireProcessor($_REQUEST['Form']);
    
    // Lets do some User checking to make sure we are ready to post
    if (!isset($GLOBALS['Session']->User)) // Check that we are logged in
        $_REQUEST['Page'] = "User-FBLogin";
    else if ($GLOBALS['Session']->User->ZipCode == 0) // Check that we have a ZipCode
    {
        $GLOBALS['Alerts'][] = new Alert("You must enter a US ZipCode to use this site!", "danger");
        $_REQUEST['Page'] = "User-Profile";
    }
    
    
    // $FBNotification = new FBNotification();
    // $FBNotification->Send();
    // die();
    
    $Page = isset($_REQUEST['Page']) ? $_REQUEST['Page'] : "Home";
    
    $GLOBALS['Page'] = new Page($Page);
    
    $GLOBALS['Page']->Render();
    
?>
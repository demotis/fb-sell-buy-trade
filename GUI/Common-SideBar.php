<h3 style="margin-top:0px; margin-bottom:30px;"><span class="label label-info col-sm-12">Notifications</span></h3>
<div class="row-fluid" style="margin-bottom: 20px; overflow: auto; height: 150px;">
<?php
    if(!isset($GLOBALS['Session']->User)) { ?>
        <h4>Please Login</h4>
    <?php } else { ?>
    
    <div class="list-group">

    <?php
    
        $notifications = (new Notifications($GLOBALS['Session']->User->FBID))->Get();
        
        if (empty($notifications))
            echo "<a href='#' class='list-group-item'>No Notifications</a>";
        
        foreach ($notifications as $notiData)
            echo "<a href='#' class='list-group-item'>Vestibulum at eros</a>";

    ?>
  
    </div>

    <?php } ?>

</div>

<h3 style="margin-top:0px; margin-bottom:30px;"><span class="label label-info col-sm-12">Search Settings</span></h3>
<div class="row-fluid">
<?php
    if(!isset($GLOBALS['Session']->User)) { ?>
        <h4>Please Login</h4>
    <?php } else { ?>

<form role="form" method="post" action="./">
    <input type="hidden" name="Form" value="ChangeSearchSettings">
    <div class="form-group">
        <label for="ZipCode">Current ZipCode</label>
        <input type="text" class="form-control" id="ZipCode" name="ZipCode" placeholder="Enter ZipCode" value="<?php echo $GLOBALS['Session']->ActiveZipCode; ?>">
        <p class="help-block">Only changes active ZipCode.</p>
    </div>
    <div class="form-group">
        <label for="Distance">Search Distance</label>
        <select class="form-control" id="Distance" name="Distance">
            <?php
                $distArray = array(5,15,25,35,55,105,-1);
                foreach ($distArray as $dist)
                {
                    $selected = $dist == $GLOBALS['Session']->ActiveDistance ? "SELECTED" : "";
                    $text = $dist > 0 ? "{$dist} Miles" : "All Listings";
                    echo "<option value='{$dist}' {$selected}>{$text}</option>";
                }
            ?>
        </select>
    </div>
    <button type="submit" class="btn btn-default pull-right">Submit</button>
</form>

    <?php } ?>

</div>
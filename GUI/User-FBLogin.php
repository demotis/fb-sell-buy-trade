<div class=container-fluid>
  <h2>Avoiding Scams Starts with You</h2>
  <p>99% of all scams depend on the scammer staying annonymous. Dealing with local people and meeting face to face will assist in preventing most scams</p>
  <p>Follow these few guidelines and keep your money safe</p>
  <ul>
    <li>Never pay anyone that you have not met in person. Sending your payment to an unknown person is never a good idea.</li>
    <li>Deal with locals that you can meet in person. Avoid the shipping cost and the risk that you might not get your package.</li>
    <li>If you do have your item shipped, use a payment processor that offers protection, such as PayPal.</li>
    <li>Never wire funds, never ask to have someone wire funds. Treat anyone that asks you to send money via Western Union, Money Gram, etc. as a scammer.</li>
    <li>Do not accept cashier / certified checks or money orders. These can be faked, and banks will cash them. When they come back as fraud the bank will hold you responseable for the funds.</li>
    <li>Never provide financial or personal information on this site, via messenger or over email.</li>
    <li>Never rent or purchase without seeing the product. Scammers sometimes try to sell things they don't own or even have.</li>
    <li>Background and Credit Checks expose your personal information. Never allow a check to be performed untill you have met the landlord or employer in person.</li>
  </ul>
</div>

<div class="container-fluid">
  <h2>Personal Safety, Protect yourself above all</h2>
  <p>Most people are not out with the intent to harm you, but should you risk your personal well being in the hope that everything will be fine?</p>
  <p>Use common sense online, not just offline. Predators can learn a lot about you without you being aware of giving it to them.</p>
  <p>Below is a list of guidlines to assist you in staying safe, but remember no one can predict all situations. It is your responsibility to stay alert and never do anything that makes you feel uncomfortable.</p>
  <ul>
    <li>Insist on meeting in a public place, where there are currently other people active. Meeting at a closed gas station in the middle of the day is unsafe.</li>
    <li>Avoid meeting in a secluded place, or inviting stangers to your home or place of business.</li>
    <li>Take care when dealing with high value items. Both selling or buying high value items makes you a target for theives and scammers.</li>
    <li>Tell someone where you are going, stay in contact with them, set a "maximum" time to call.</li>
    <li>If you have a cell phone, TAKE IT. If it is a smart phone install a tracking software such as Life360 and have someone monitor it.</li>
    <li>Whenever possible, take someone with you, a friend, family member, large dog anything is better than being alone when meeting a stranger.</li>
    <li>Trust your instincts, if something doesn't feel right then don't do it. Just leave, you can sell to the next buyer or find another great deal.</li>  </ul>
</div>

<div class="container-fluid">
  <h2>Prohibited Item List</h2>
  <div class="alert alert-danger" role="alert">We reserve the right to adjust this list without warning. It is your responsablity to ensure that you are compliant with changes to this list.</div>
  <p>We never want to tell you what you can and can not buy or sell. However, there are a few things that we ask you not to list or ask for.</p>
  <p>If something in your area is illegal, do not list or try to sell. While we do not track local laws we will comply with all law enforcement agencys for requests of information pertaining to dealings on this site.</p>
  <ul>
    <li>Recalled or Hazardous Meterials</li>
    <li>Body Parts or Body Fluids</li>
    <li>Medications, prescription or over the counter</li>
    <li>Controlled Substances as listed by Federal Law</li>
    <li>Child Pornography, Bestiality, Illegal Prostitution</li>
  </ul>
</div>
  





<form id="fbLoginForm" action="./" method="post">
    <input type="hidden" name="Form" value="FBLogin">
    <input type="hidden" name="FbId" id="FbId" value="">
    <input type="hidden" name="EMail" id="EMail" value="">
    <input type="hidden" name="FirstName" id="FirstName" value="">
    <input type="hidden" name="LastName" id="LastName" value="">
</form>

<div id='fb-root'></div>

<script>
   
   

</script>

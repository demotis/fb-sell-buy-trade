<?php
    $listing = new Listing($_REQUEST['lid']);
    
    echo $listing->DisplayTitle(); 
    echo $listing->CategoryData->GetBreadCrumb(); 
    
?>


<div class='container-fluid'>

    <div class='row panel' style='padding: 5px; margin-bottom: 10px;'>

        <!-- Image Slider -->
        <div id="listingImages" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                    for ($i = 0; $i < count($listing->GetImageURLs()); $i++)
                    {
                        $active = $i == 0 ? "active" : "";
                        echo "<li data-target='#listingImages' data-slide-to='{$i}' class='{$active}'></li>";
                    }
                ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php
                    for ($i = 0; $i < count($listing->GetImageURLs()); $i++)
                    {
                        $image = $listing->GetImageURLs()[$i];
                        $active = $i == 0 ? "active" : "";
                        echo "<div class='item {$active}' data-featherlight='{$image}'><img src='{$image}' alt='' class='img-responsive' /><div class='carousel-caption'>Click for Full Size View</div></div>";
                    }
                ?>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#listingImages" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#listingImages" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- END Image Slider -->
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <?php echo $listing->Description; ?>
                </div>
                <div class="col-md-4">
                    <div class="row panel panel-primary" style="padding: 5px;">
                        <div class="row-fluid">
                            <h3 class="no-margin">Asking<span class='badge pull-right col-sm-6'>$ <?php echo number_format($listing->Price, 2); ?></span></h3>
                        </div>
                        <div class="row-fluid">
                            <h3 class="no-margin">Offered<span class='badge pull-right col-sm-6'>$ <?php echo number_format($listing->Offer, 2); ?></span></h3>
                        </div>
                        
                        <!-- Offer Details - Start -->
                        <div class="row-fluid">
                            <?php echo $listing->GetOfferDetails() ?>
                        </div>
                        <!-- Offer Details - End -->
                        
                        <!-- Make Offer - Start -->
                        <div class="row-fluid">
                            <button class="btn btn-warning col-sm-12" data-toggle="modal" data-target="#OfferModal">Make Offer</button>

                            <!-- Make offer Modal - Start -->
                            <div class="modal fade" id="OfferModal" tabindex="-1" role="dialog" aria-labelledby="OfferModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Make Offer</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="alert alert-danger" role="alert">
                                                <p>You are making an offer to trade for goods or services as described. If you have any questions about the listing ask them before you make an offer.</p>
                                                <p>This offer will be considered a non-binding promiss to trade with the lister. While you have the right to resend an offer, a claim can be submitted against your account. Excessive claims can result in the tempory or perminate banning of your access to this service.</p>
                                            <p>Only make offers that you are willing and prepared to follow through with.</p>
                                            </div>
                                            <form class="form-inline" role="form" method="post" action="./">
                                                <input type="hidden" name="Form" value="MakeOffer">
                                                <input type="hidden" name="lid" value="<?php echo $_REQUEST['lid']; ?>">
                                                <div class="form-group">
                                                    <label class="" for="offerAmount">Amount (in dollars)</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <input type="number" min="0" step="1" class="form-control text-right"  id="offerAmount" name="offerAmount" placeholder="Enter Offer Amount" value="<?php echo $listing->Offer + 1; ?>">
                                                        <div class="input-group-addon">.00</div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-danger pull-right">Make Offer</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Make offer Modal - End -->

                        </div>
                        <!-- Make Offer - End -->
                        
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class='row'>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Comments</h3>
            </div>
            <div class="panel-body">
                <table class="table table-condensed">
                    <tbody>
                        <?php echo $listing->DisplayComments() ?>
                    </tbody>
                </table>
                <form class="form" role="form" method="post" action="./">
                    <input type="hidden" name="Form" value="PostListingComment">
                    <input type="hidden" name="lid" value="<?php echo $_REQUEST['lid']; ?>">
                    <div class="input-group">
                        <div class="input-group-addon"><img src="<?php echo $GLOBALS['Session']->User->GetFBImageUrl(); ?>" /></div>
                        <textarea class="form-control"  id="comment" name="comment" placeholder="Enter Comment" style="height: 64px;"></textarea>
                        <div class="input-group-addon"><button type="submit" class="btn btn-success">Post</button></div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>

    <div>

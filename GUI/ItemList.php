<?php

    if (isset($_REQUEST['CatID']))
        $GLOBALS['Session']->SetActiveSearchCategoryID($_REQUEST['CatID']);
    $CategoryData = new CategoryData($GLOBALS['Session']->ActiveSearchCategoryID);
        
?>

<h3 style="margin-top:0px;"><span class="label label-info col-sm-12">Listings</span></h3>
<?php echo $CategoryData->GetBreadCrumb(); ?>


<?php echo $CategoryData->GetListings(); ?>
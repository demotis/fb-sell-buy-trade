<?php

    
?>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        
        <?php
            // Just display a heading for login
            if (!isset($GLOBALS['Session']->User))
                echo '<a class="navbar-brand" href="#">Warning - Please Read</a>';
        ?>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

    <?php
        // If the User is set then display the Catagory Nav
        if (isset($GLOBALS['Session']->User)) { ?>
                <ul class="nav navbar-nav">
                    
                    <?php
                        $catData = new Categories();
                        foreach ($catData->GetRoots() as $catRoot) { ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $catRoot['Label']; ?> <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <?php foreach ($catData->GetChildren($catRoot['ID']) as $child) { ?>
                                        <li><a href="?Page=ItemList&CatID=<?php echo $child['ID']; ?>"><?php echo $child['Label']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php }
                    ?>

                </ul>
      
        <?php } ?>
          
        <ul class="nav navbar-nav navbar-right">
    
            <?php
                // Display the Login Button or the User Menu
                if (isset($GLOBALS['Session']->User)) { ?>
        
                    <li><button type="button" class="btn btn-danger navbar-btn" onclick="window.location.href = '?Page=CreateListing';">Create New Listing</button></li>
        
                <?php } else if ($GLOBALS['Page']->BodyTemplate == 'User-FBLogin') { ?>

                    <li><button class="btn btn-warning navbar-btn" id="loginBtn" disabled="disabled" onclick="$('#fbLoginForm').submit();">Continue to Page</button></li>

                <?php } ?>
        </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->


</nav>
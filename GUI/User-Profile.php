<h1>Edit User Profile</h1>

<form class="form-horizontal" role="form" method="post" action="./">
    <input type="hidden" name="Form" value="UpdateUser">
  <div class="form-group">
    <label for="FirstName" class="col-sm-2 control-label">First Name</label>
    <div class="col-sm-10 col-md-8">
      <input type="text" class="form-control" id="FirstName" name="FirstName" placeholder="First Name" value="<?php echo $GLOBALS['Session']->User->FirstName; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="LastName" class="col-sm-2 control-label">Last Name</label>
    <div class="col-sm-10 col-md-8">
      <input type="text" class="form-control" id="LastName" name="LastName" placeholder="Last Name" value="<?php echo $GLOBALS['Session']->User->LastName; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="EMail" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10 col-md-8">
      <input type="email" class="form-control" id="EMail" name="EMail" placeholder="Email" value="<?php echo $GLOBALS['Session']->User->EMail; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="ZipCode" class="col-sm-2 control-label">ZipCode</label>
    <div class="col-sm-10 col-md-8">
      <input type="text" class="form-control" id="ZipCode" name="ZipCode" placeholder="Enter 5 Digit ZipCode" value="<?php echo $GLOBALS['Session']->User->ZipCode; ?>">
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-12 col-md-8 col-md-offset-2 ">
      <button type="submit" class="btn btn-default pull-right">Save</button>
    </div>
  </div>
</form>
<?php
    

?>

<form id="SubmitListing" role="form" enctype="multipart/form-data" action="./" method="post">
    <input type="hidden" name="Form" value="CreateListing" />
    <input type="hidden" name="Page" value="Home" />
    <div class="form-group">
        <label for="CatID">Select Category</label>
        <select class="form-control" id="CatID" name="CatID">
            <option value="-1">Select Category</option>
        <?php
            $catData = new Categories();
            foreach ($catData->GetRoots() as $catRoot) { ?>
                <optgroup label="<?php echo $catRoot['Label']; ?>">
                <?php foreach ($catData->GetChildren($catRoot['ID']) as $child) { ?>
                    <option value="<?php echo $child['ID']; ?>"><?php echo $child['Label']; ?></option>
                <?php } ?>
                </optgroup>
            <?php }
        ?>
        </select>
    </div>
    <div class="form-group">
        <label for="Title">Title</label>
        <input type="text" class="form-control" id="Title" name="Title" placeholder="Listing Title">
    </div>

    <div class="form-group">
        <label for="Description">Description</label>
        <textarea class="form-control" rows="80" id="Description" name="Description" ></textarea>
    </div>

    <div class="form-group">
        <label for="file[]">Image Upload</label>
        <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-primary btn-file">
                    Browse… <input type="file" name="file[]" accept="image/*" multiple>
                </span>
            </span>
            <input type="text" class="form-control" readonly="">
        </div>
        <span class="help-block">
            Multiple file selection is allowed, Max file size 5 Mb.
        </span>
    </div>

    <div class="form-group">
        <label for="Price">Price</label>
        <div class="input-group">
            <div class="input-group-addon">$</div>
            <input type="number" min="0" step="1" class="form-control text-right" id="Price" name="Price" placeholder="Amount">
            <div class="input-group-addon">.00</div>
        </div>
    </div>

</form>

    <button class="btn btn-default pull-right" onClick="SubmitListing();">Add Listing</button>


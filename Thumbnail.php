<?php
 
$sImagePath = "PageImages/" . $_GET["img"];

$iMaxWidth = 100;
$iMaxHeight = 100;
$sType = 'scale';

$img = NULL;
 
$sExtension = strtolower(end(explode('.', $sImagePath)));
if ($sExtension == 'jpg' || $sExtension == 'jpeg') {
 
    $img = @imagecreatefromjpeg($sImagePath)
        or die("Cannot create new JPEG image");
 
} else if ($sExtension == 'png') {
 
    $img = @imagecreatefrompng($sImagePath)
        or die("Cannot create new PNG image");
 
} else if ($sExtension == 'gif') {
 
    $img = @imagecreatefromgif($sImagePath)
        or die("Cannot create new GIF image");
 
}
 
if ($img) {
 
    $iOrigWidth = imagesx($img);
    $iOrigHeight = imagesy($img);

    $fScale = min($iMaxWidth/$iOrigWidth, $iMaxHeight/$iOrigHeight);
    if ($fScale < 1) {
        $iNewWidth = floor($fScale*$iOrigWidth);
        $iNewHeight = floor($fScale*$iOrigHeight);
        $tmpimg = imagecreatetruecolor($iNewWidth, $iNewHeight);
 
        imagecopyresampled($tmpimg, $img, 0, 0, 0, 0,
        $iNewWidth, $iNewHeight, $iOrigWidth, $iOrigHeight);
 
        imagedestroy($img);
        $img = $tmpimg;
    }     

 
    header("Content-type: image/jpeg");
    imagejpeg($img);
 
}
 
?>
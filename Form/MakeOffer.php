<?php 

    // Create a new Listing
    $listing = new Listing($_REQUEST['lid']);

    if (!$listing->MakeOffer($_REQUEST['offerAmount']))
        $GLOBALS['Alerts'][] = new Alert("Your offer was not more than the current offer, Try Again?", "warning");

    $_REQUEST['Page'] = "ShowListing";
    $_REQUEST['lid'] = $listing->ID;
<?php

    // Validate the form
    

    // Create a new Listing
    $listing = new Listing();
    $listing->CategoryID = $_REQUEST['CatID'];
    $listing->Title = $_REQUEST['Title'];
    $listing->Description = $_REQUEST['Description'];
    $listing->Price = $_REQUEST['Price'];


    $j = 0;
    $BasePath = "PageImages/";
    for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
        if (empty($_FILES['file']['name'][$i])) continue;
        $validextensions = array("jpeg", "jpg", "png");
        $ext = explode('.', basename($_FILES['file']['name'][$i]));   // Explode file name from dot(.)
        $file_extension = end($ext); // Store extensions in the variable.
        $uniqueFileName = md5(uniqid()) . "." . $ext[count($ext) - 1];
        $target_path = $BasePath . $uniqueFileName;     // Set the target path with a new name of image.
        $j = $j + 1;      // Increment the number of uploaded images according to the files in array.

        if (($_FILES["file"]["size"][$i] < 5000000) && in_array($file_extension, $validextensions)) {
            if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $target_path))
                $listing->AddImage($uniqueFileName);
        }
        else     //   If File Size And File Type Was Incorrect.
            $GLOBALS['Alerts'][] = new Alert("Upload of File Failed, Invalid Size or Type :: {$_FILES['file']['name'][$i]}", "danger");
    }
    
    $listing->Save();
    
    $_REQUEST['Page'] = "ShowListing";
    $_REQUEST['lid'] = $listing->ID;

/*
    var_dump($_REQUEST);
    var_dump($_FILES);
    
    array(6) { 
        ["Form"]=> string(13) "CreateListing" 
        ["Page"]=> string(4) "Home" 
        ["CatID"]=> string(2) "17" 
        ["Title"]=> string(22) "This is a Fake Listing" 
        ["Description"]=> string(34) "This is the body of a fake listing" 
        ["Price"]=> string(6) "100.00"
        
    }
    
    array(1) { 
        ["file"]=> array(5) { 
            ["name"]=> array(3) { 
                [0]=> string(13) "bg-footer.jpg" 
                [1]=> string(18) "bg-header-full.png" 
                [2]=> string(17) "fb_button_180.png" 
            } 
            ["type"]=> array(3) { 
                [0]=> string(10) "image/jpeg" 
                [1]=> string(9) "image/png" 
                [2]=> string(9) "image/png" 
            } 
            ["tmp_name"]=> array(3) { 
                [0]=> string(14) "/tmp/phpbvJFo2" 
                [1]=> string(14) "/tmp/php8ckNhp" 
                [2]=> string(14) "/tmp/phpF189aM" 
            } 
            ["error"]=> array(3) { 
                [0]=> int(0) 
                [1]=> int(0) 
                [2]=> int(0) 
            } ["size"]=> array(3) { 
                [0]=> int(54675) 
                [1]=> int(166417) 
                [2]=> int(5346) 
            } 
        } 
    }
*/
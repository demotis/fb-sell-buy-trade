<?php

    if (!isset($GLOBALS['Session']->User)) // Check that we are logged in
        return;
    
    if ($GLOBALS['Session']->ActiveZipCode != $_REQUEST['ZipCode'])
        $GLOBALS['Session']->SetActiveZipCode($_REQUEST['ZipCode']);
        
    if ($GLOBALS['Session']->ActiveDistance != $_REQUEST['Distance'])
        $GLOBALS['Session']->SetActiveDistance($_REQUEST['Distance']);
        
    $_REQUEST['Page'] = "ItemList";

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '380256168810457',
      xfbml      : true,
      cookie     : true,
      version    : 'v2.2'
    });

    // ADD ADDITIONAL FACEBOOK CODE HERE
    function onLogin(response) {
      if (response.status == 'connected') {
        FB.api('/me?fields=id,first_name,last_name,email', function(data) {
          $('#FbId').val(data.id);
          $('#EMail').val(data.email);
          $('#FirstName').val(data.first_name);
          $('#LastName').val(data.last_name);
          $('#loginBtn').removeAttr('disabled');
        });
      }
    }

    FB.getLoginStatus(function(response) {
      // Check login status on load, and if the user is
      // already logged in, go directly to the welcome message.
      if (response.status == 'connected') {
        onLogin(response);
      } else {
        // Otherwise, show Login dialog first.
        FB.login(function(response) {
          onLogin(response);
        }, {scope: 'user_friends, email'});
      }
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

<?php

// FB Requires
// Loaded from a seperate script to include all SDK files

// FB NameSpace
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookJavaScriptLoginHelper;

FacebookSession::setDefaultApplication($GLOBALS['FB_AppID'], $GLOBALS['FB_AppSecret']);

$helper = new FacebookJavaScriptLoginHelper();
try {
    $GLOBALS['FB_Session'] = $helper->getSession();
} catch(FacebookRequestException $ex) {
    // When Facebook returns an error
} catch(\Exception $ex) {
    // When validation fails or other local issues
}